FROM debian

ENV DDBMS=pgsql

RUN apt-get update \
  && apt-get install -y \
    build-essential \
    dh-autoreconf \
    cmake \
    postgresql \
    libpq-dev \
    postgresql-server-dev-11 \
    r-base \
    ghostscript \
    git \
  && git clone https://git.code.sf.net/p/osdldbt/dbt3 /osdldbt-dbt3

RUN cd osdldbt-dbt3 \
  && sh autogen.sh \
  && ./configure --with-postgresql \
  && make \
  && make install \
  && useradd -ms /bin/bash unpriv

USER unpriv
WORKDIR /osdldbt-dbt3
ADD ./docker-entrypoint.sh /docker-entrypoint.sh
ADD ./dbt3-pgsql-load-data /usr/local/bin/dbt3-pgsql-load-data
ADD ./dbt3-pgsql-power-test /usr/local/bin/dbt3-pgsql-power-test
ADD ./dbt3-pgsql-throughput-test /usr/local/bin/dbt3-pgsql-throughput-test

ENV \
  DSS_QUERY="/osdldbt-dbt3/queries/pgsql" \
  DSS_PATH="/tmp/dss" \
  DSS_CONFIG="/osdldbt-dbt3/src/dbgen" \
  DBGEN="/usr/local/bin/dbgen" \
  QGEN="/usr/local/bin/qgen" \
  DBT3_DIR="/tmp/dbt3" \
  PGDATABASE="dbt3" \
  PGDATA="${DBT3_DIR}/pgdata" \
  PGPORT=5432 \
  DEFAULT_LOAD_PARAMETERS="" \
  DEFAULT_POWER_PARAMETERS="" \
  DEFAULT_THROUGHPUT_PARAMETERS="" \
  THROUGHPUT_STREAMS="4" \
  TSDIR="${DBT3_DIR}" \
  SCALE_FACTOR_GB="1" \
  RESULTS_DIR="/tmp/results"

ENTRYPOINT ["/docker-entrypoint.sh"]

