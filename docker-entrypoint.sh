#!/bin/bash
set -e

cat <<EOF > $HOME/.pgpass
${PGHOST}:${PGPORT:=5432}:${PGDATABASE}:${PGUSER}:${PGPASSWORD}
EOF
export PGPASSFILE="${PGPASSFILE:=$HOME/.pgpass}"

# ensure DSS_PATH exists
mkdir -p "${DSS_PATH}" || true
mkdir -p "${RESULTS_DIR}" || true

echo "[dbgen] generating ${SCALE_FACTOR_GB}GB data into ${DSS_PATH}"
dbgen -s $SCALE_FACTOR_GB

echo "[createdb] creating database '$PGDATABASE'"
createdb $PGDATABASE || true

echo "[dbt3-pgsql-create-tables] creating tables"
dbt3-pgsql-create-tables || true

echo "[dbt3-pgsql-dbstat] starting database statistics collection"
dbt3-pgsql-dbstat $RESULTS_DIR 2>/dev/null &

echo "[dbt3-pgsql-load-data]"
dbt3-pgsql-time-statistics -s -n LOAD
dbt3-pgsql-load-data -C 1
dbt3-pgsql-time-statistics -e -n LOAD

echo "[dbt3-pgsql-power-test]"
for i in $(seq 1 22); do
  dbt3-pgsql-time-statistics -s -n POWER${i}
  dbt3-pgsql-power-test "$(date +%-m%d%H%M%S)" "${i}"
  dbt3-pgsql-time-statistics -e -n POWER${i}
done

echo "[dbt3-throughput-test]"
for i in $(seq 1 22); do
  dbt3-pgsql-time-statistics -s -n THROUGHPUT${i}
  dbt3-pgsql-throughput-test "$(date +%-m%d%H%M%S)" "${i}" "${THROUGHPUT_STREAMS}"
  dbt3-pgsql-time-statistics -e -n THROUGHPUT${i}
done

echo "[dbt3-pgsql-dbstat] stopping database statistics collection"
if [ -f "$RESULTS_DIR/dbstat.pid" ]; then
  kill "$(cat $RESULTS_DIR/dbstat.pid)"
fi

